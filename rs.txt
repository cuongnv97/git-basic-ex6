- fork repo sau do clone source code về máy
	git clone https://gitlab.com/cuongnv97/git-basic-ex6.git

- điều hướng vào thư mục git-basic-ex6
	cd git-basic-ex6

- chuyển sang nhánh f1
	git checkout f1

- kiểm tra log commit của nhánh để lấy commit id của accident commit
	git log

- undo lại sau commit accident commit
	git revert ea7b37c6
